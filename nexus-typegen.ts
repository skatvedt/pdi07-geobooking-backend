/**
 * This file was generated by Nexus Schema
 * Do not make changes to this file directly
 */


import type { Context } from "./src/api/context"
import type { core } from "nexus"
declare global {
  interface NexusGenCustomInputMethods<TypeName extends string> {
    /**
     * A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar.
     */
    datetime<FieldName extends string>(fieldName: FieldName, opts?: core.CommonInputFieldConfig<TypeName, FieldName>): void // "DateTime";
  }
}
declare global {
  interface NexusGenCustomOutputMethods<TypeName extends string> {
    /**
     * A date-time string at UTC, such as 2007-12-03T10:15:30Z, compliant with the `date-time` format outlined in section 5.6 of the RFC 3339 profile of the ISO 8601 standard for representation of dates and times using the Gregorian calendar.
     */
    datetime<FieldName extends string>(fieldName: FieldName, ...opts: core.ScalarOutSpread<TypeName, FieldName>): void // "DateTime";
  }
}


declare global {
  interface NexusGen extends NexusGenTypes {}
}

export interface NexusGenInputs {
}

export interface NexusGenEnums {
}

export interface NexusGenScalars {
  String: string
  Int: number
  Float: number
  Boolean: boolean
  ID: string
  DateTime: any
}

export interface NexusGenObjects {
  Booking: { // root type
    created_at: NexusGenScalars['DateTime']; // DateTime!
    end_date: NexusGenScalars['DateTime']; // DateTime!
    room_uuid?: string | null; // String
    start_date: NexusGenScalars['DateTime']; // DateTime!
    user_email?: string | null; // String
  }
  Floor: { // root type
    floor_id?: string | null; // String
    floor_number?: number | null; // Int
  }
  Mutation: {};
  Query: {};
  Room: { // root type
    domaine?: string | null; // String
    floor_id?: string | null; // String
    floor_number?: number | null; // Int
    id?: number | null; // Int
    n_bureau?: string | null; // String
    n_salle?: string | null; // String
    nom?: string | null; // String
    shape_area?: number | null; // Float
    shape_leng?: number | null; // Float
    uuid?: string | null; // String
  }
}

export interface NexusGenInterfaces {
}

export interface NexusGenUnions {
}

export type NexusGenRootTypes = NexusGenObjects

export type NexusGenAllTypes = NexusGenRootTypes & NexusGenScalars

export interface NexusGenFieldTypes {
  Booking: { // field return type
    created_at: NexusGenScalars['DateTime']; // DateTime!
    end_date: NexusGenScalars['DateTime']; // DateTime!
    room_uuid: string | null; // String
    rooms: Array<NexusGenRootTypes['Room'] | null>; // [Room]!
    start_date: NexusGenScalars['DateTime']; // DateTime!
    user_email: string | null; // String
  }
  Floor: { // field return type
    floor_id: string | null; // String
    floor_number: number | null; // Int
    rooms: Array<NexusGenRootTypes['Room'] | null>; // [Room]!
  }
  Mutation: { // field return type
    createBooking: NexusGenRootTypes['Booking'] | null; // Booking
  }
  Query: { // field return type
    bookings: Array<NexusGenRootTypes['Booking'] | null>; // [Booking]!
    floors: Array<NexusGenRootTypes['Floor'] | null>; // [Floor]!
    rooms: Array<NexusGenRootTypes['Room'] | null>; // [Room]!
  }
  Room: { // field return type
    domaine: string | null; // String
    floor: NexusGenRootTypes['Floor'] | null; // Floor
    floor_id: string | null; // String
    floor_number: number | null; // Int
    geom: string | null; // String
    id: number | null; // Int
    n_bureau: string | null; // String
    n_salle: string | null; // String
    nom: string | null; // String
    shape_area: number | null; // Float
    shape_leng: number | null; // Float
    uuid: string | null; // String
  }
}

export interface NexusGenFieldTypeNames {
  Booking: { // field return type name
    created_at: 'DateTime'
    end_date: 'DateTime'
    room_uuid: 'String'
    rooms: 'Room'
    start_date: 'DateTime'
    user_email: 'String'
  }
  Floor: { // field return type name
    floor_id: 'String'
    floor_number: 'Int'
    rooms: 'Room'
  }
  Mutation: { // field return type name
    createBooking: 'Booking'
  }
  Query: { // field return type name
    bookings: 'Booking'
    floors: 'Floor'
    rooms: 'Room'
  }
  Room: { // field return type name
    domaine: 'String'
    floor: 'Floor'
    floor_id: 'String'
    floor_number: 'Int'
    geom: 'String'
    id: 'Int'
    n_bureau: 'String'
    n_salle: 'String'
    nom: 'String'
    shape_area: 'Float'
    shape_leng: 'Float'
    uuid: 'String'
  }
}

export interface NexusGenArgTypes {
  Mutation: {
    createBooking: { // args
      end_date: number; // Float!
      start_date: number; // Float!
      user_email: string; // String!
    }
  }
}

export interface NexusGenAbstractTypeMembers {
}

export interface NexusGenTypeInterfaces {
}

export type NexusGenObjectNames = keyof NexusGenObjects;

export type NexusGenInputNames = never;

export type NexusGenEnumNames = never;

export type NexusGenInterfaceNames = never;

export type NexusGenScalarNames = keyof NexusGenScalars;

export type NexusGenUnionNames = never;

export type NexusGenObjectsUsingAbstractStrategyIsTypeOf = never;

export type NexusGenAbstractsUsingStrategyResolveType = never;

export type NexusGenFeaturesConfig = {
  abstractTypeStrategies: {
    isTypeOf: false
    resolveType: true
    __typename: false
  }
}

export interface NexusGenTypes {
  context: Context;
  inputTypes: NexusGenInputs;
  rootTypes: NexusGenRootTypes;
  inputTypeShapes: NexusGenInputs & NexusGenEnums & NexusGenScalars;
  argTypes: NexusGenArgTypes;
  fieldTypes: NexusGenFieldTypes;
  fieldTypeNames: NexusGenFieldTypeNames;
  allTypes: NexusGenAllTypes;
  typeInterfaces: NexusGenTypeInterfaces;
  objectNames: NexusGenObjectNames;
  inputNames: NexusGenInputNames;
  enumNames: NexusGenEnumNames;
  interfaceNames: NexusGenInterfaceNames;
  scalarNames: NexusGenScalarNames;
  unionNames: NexusGenUnionNames;
  allInputTypes: NexusGenTypes['inputNames'] | NexusGenTypes['enumNames'] | NexusGenTypes['scalarNames'];
  allOutputTypes: NexusGenTypes['objectNames'] | NexusGenTypes['enumNames'] | NexusGenTypes['unionNames'] | NexusGenTypes['interfaceNames'] | NexusGenTypes['scalarNames'];
  allNamedTypes: NexusGenTypes['allInputTypes'] | NexusGenTypes['allOutputTypes']
  abstractTypes: NexusGenTypes['interfaceNames'] | NexusGenTypes['unionNames'];
  abstractTypeMembers: NexusGenAbstractTypeMembers;
  objectsUsingAbstractStrategyIsTypeOf: NexusGenObjectsUsingAbstractStrategyIsTypeOf;
  abstractsUsingStrategyResolveType: NexusGenAbstractsUsingStrategyResolveType;
  features: NexusGenFeaturesConfig;
}


declare global {
  interface NexusGenPluginTypeConfig<TypeName extends string> {
  }
  interface NexusGenPluginInputTypeConfig<TypeName extends string> {
  }
  interface NexusGenPluginFieldConfig<TypeName extends string, FieldName extends string> {
  }
  interface NexusGenPluginInputFieldConfig<TypeName extends string, FieldName extends string> {
  }
  interface NexusGenPluginSchemaConfig {
  }
  interface NexusGenPluginArgConfig {
  }
}