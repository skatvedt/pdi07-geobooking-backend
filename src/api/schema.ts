// api/schema.ts
import { join } from "path";
import path from "path";
import { fileURLToPath } from "url";
import { asNexusMethod, makeSchema } from "nexus";
import * as types from "./index.js";
import { DateTimeResolver } from "graphql-scalars";

const filename = fileURLToPath(import.meta.url); // Get file URL path
const dirname = path.dirname(filename); // Get directory path
export const DateTime = asNexusMethod(DateTimeResolver, "datetime"); // Create DateTime custom scalar

// The makeSchema function generates a GraphQL schema by combining the provided
// Nexus types, custom scalars, and other configurations. It also generates
// associated TypeScript typings (typegen) and outputs the schema definition as
// a GraphQL file (schema.graphql), which can be used for various purposes like
// client code generation, schema validation, or schema documentation.
export const schema = makeSchema({
  types: [types, DateTime], // Include types and custom DateTime scalar
  outputs: {
    typegen: join(dirname, "../..", "nexus-typegen.ts"), // Output typegen file
    schema: join(dirname, "../..", "schema.graphql"), // Output schema file
  },
  contextType: {
    // 1
    module: join(dirname, "./context.ts"), // 2 Import context module
    export: "Context", // 3 Export the "Context" type
  },
});
