import {
  objectType,
  extendType,
  list,
  nonNull,
  stringArg,
  intArg,
} from "nexus";

// This is where we define the Floor type.
export const Floor = objectType({
  name: "Floor", // <- Name of your type
  definition(t) {
    t.string("floor_id");
    t.int("floor_number");
    t.field("rooms", {
      type: nonNull(list("Room")),
      resolve(_root, _args, ctx) {
        // This is a resolver function for the "rooms" field that returns all rooms on the given floor.
        return ctx.db.rooms.findMany({ where: { floor_id: _root.floor_id } });
      },
    });
  },
});

export const FloorQuery = extendType({
  type: "Query",
  definition(t) {
    t.field("floors", {
      type: nonNull(list("Floor")),
      resolve(_root, _args, ctx) {
        // This query returns all floors in the database.
        return ctx.db.floors.findMany();
      },
    });
  },
});
/*
export const PostMutation = extendType({
  type: "Mutation",
  definition(t) {
    t.nonNull.field("createDraft", {
      type: "Post",
      args: {
        // 1
        title: nonNull(stringArg()), // 2
        body: nonNull(stringArg()), // 2
      },
      resolve(_root, args, ctx) {
        const draft = {
          title: args.title, // 3
          body: args.body, // 3
          published: false,
        };
        return ctx.db.post.create({ data: draft });
      },
    });
    t.field("publish", {
      type: "Post",
      args: {
        draftId: nonNull(intArg()),
      },
      resolve(_root, args, ctx) {
        return ctx.db.post.update({
          where: { id: args.draftId },
          data: {
            published: true,
          },
        });
      },
    });
  },
});
 */
