import { ApolloServer } from "@apollo/server";
import { schema } from "./schema.js";

// Instantiate an ApolloServer object by passing the schema.
// This creates a new instance of the Apollo GraphQL server using the given schema.
export const server = new ApolloServer({ schema });
