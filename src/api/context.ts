// api/context.ts
import { db } from "./db.js";
import { PrismaClient } from "@prisma/client";

// This is where you define the context type for your application, in our case the PrismaClient.
// It is an auto-generated client that enables developers to interact with their databases
// using a type-safe and developer-friendly API. PrismaClient is used to send queries to the
// database, perform CRUD operations, and handle transactions.
export interface Context {
  db: PrismaClient;
}
export const context = {
  db,
};
