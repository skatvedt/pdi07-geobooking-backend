# PDI07 GeoBooking Backend

Welcome to the PDI07 GeoBooking Backend repository. This project is a GraphQL API using Apollo Server, Nexus.js as a schema builder, Prisma ORM, and PostgreSQL as a database. The application can be built and started using Docker (Docker compose).

> **Warning** — This repo only contains the backend/server and not the client interface. Please check our [front-end gitlab page](https://gitlab.com/clelia_s/pdi07-geobooking-mobileapp.git) here about our user interface! 



## Table of Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Running the Application](#running-the-application)
- [Usage](#usage)
- [Who we are](#who-we-are)
- [Acknowledgements](#acknowledgements)

## Requirements

- Docker
- Docker Compose

## Installation

You need to create a file in the root directory called .env which contain you environment variables. If you use the Docker database, the contents should be:

```
DATABASE_URL="postgresql://postgres:postgres@db:5432/postgres?schema=public"
```

Clone the repository:

```
git clone https://gitlab.com/skatvedt/pdi07-geobooking-backend.git
cd pdi07-geobooking-backend
```

Build the Docker containers:

```
docker-compose build
```

## Running the Application

There is an SQL backup file in the folder called SQL which contains the full database, and this SQL file is executed when Docker builds new containers.

Start the Docker containers:

```
docker-compose up
```

This will start the Apollo Server at `http://localhost:4000/` and the PostgreSQL database at port `5432`. You can access the GraphQL playground at `http://localhost:4000/`.

## Usage

To interact with the API, you can use tools like [GraphQL Playground](https://github.com/prisma/graphql-playground), [Insomnia](https://insomnia.rest/), or [Postman](https://www.postman.com/).

## Who we are

We are a team of 4 students pursuing an Undergraduate Master's degree in Geomatics Engineering at the France National School of Geographic Sciences (ENSG Géomatique).

This work is part of the IT development project scheduled in our second year subjects, and is to be presented during our GéoDev2 public demonstration.


## Acknowledgements


We would like to express our gratitude to the following people projects and their contributors, without which this project would not have been possible:


- Adrien from Web Géo Services - for providing the necessary assistance to help us get started with Woosmap
- Valentin from Coexya SAS - our project sponsor, for being helpful and responsive during the whole project
- Victor from ENSG - the IT department's leader, for providing the ENSG's plan to map our school
-  the ENSG, for suggesting this project's topic and allowing us to learn more about full-stack development!


- [Node.js](https://nodejs.org/): As the backbone of our server-side JavaScript environment, Node.js enables us to build powerful and scalable APIs.
- [TypeScript](https://www.typescriptlang.org/): By adding strong typing and additional features to JavaScript, TypeScript has significantly improved our development experience and code maintainability.
- [Apollo Server](https://www.apollographql.com/docs/apollo-server/): A fully-featured GraphQL server with a focus on ease of use, performance, and flexibility, allowing us to build a robust and efficient API.
- [Nexus.js](https://nexusjs.org/): A declarative, code-first schema construction library that streamlines our GraphQL schema creation, while maintaining type safety and code organization.
- [Prisma](https://www.prisma.io/): A powerful ORM that provides type-safe database access, ensuring a high level of productivity, and reducing the likelihood of errors when interacting with our PostgreSQL database.
- [PostgreSQL](https://www.postgresql.org/): As our primary data store, PostgreSQL offers powerful, enterprise-class features and performance, ensuring a reliable and scalable solution for our application.
- [Docker](https://www.docker.com/): For enabling us to containerize our application, providing an efficient, reproducible, and portable environment for deployment and development.
- [Docker Compose](https://docs.docker.com/compose/): For simplifying the management of our multi-container Docker applications, allowing us to define and orchestrate our services easily.

[<img src="/img/logoUGE.png"  width="20%" height="20%">](https://www.univ-gustave-eiffel.fr/) &nbsp; &nbsp; &nbsp;
[<img src="/img/logo_ENSG.png"  width="10%" height="10%">](https://ensg.eu/fr) &nbsp; &nbsp; &nbsp;
[<img src="/img/logo_coexya.png"  width="20%" height="20%">](https://www.coexya.eu/) &nbsp; &nbsp; &nbsp;
[<img src="/img/logo_woosmap.png"  width="20%" height="20%">](https://www.woosmap.com/fr)
