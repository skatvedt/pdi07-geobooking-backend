import * as NexusCore from 'nexus/dist/core'

//
//
// TYPES
// TYPES
// TYPES
// TYPES
//
//

// Models

/**
  * Generated Nexus `objectType` configuration based on your Prisma schema's model `floors`.
  *
  * ### ️⚠️ You have not written documentation for model floors
  *
  * Replace this default advisory JSDoc with your own documentation about model floors
  * by documenting it in your Prisma schema. For example:
  *
  * ```prisma
  * /// Lorem ipsum dolor sit amet...
  * model floors {
  *   foo  String
  * }
  * ```
  *
  * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
  *
  * @example
  *
  * import { objectType } from 'nexus'
  * import { floors } from 'nexus-prisma'
  *
  * objectType({
  *   name: floors.$name
  *   description: floors.$description
  *   definition(t) {
  *     t.field(floors.id)
  *   }
  * })
  */
export interface floors {
  $name: 'floors'
  $description: undefined
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `floors.floor_id`.
    *
    * ### ️⚠️ You have not written documentation for model floors
    *
    * Replace this default advisory JSDoc with your own documentation about model floors
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model floors {
    *   /// Lorem ipsum dolor sit amet.
    *   floor_id  String
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { floors } from 'nexus-prisma'
    *
    * objectType({
    *   name: floors.$name
    *   description: floors.$description
    *   definition(t) {
    *     t.field(floors.floor_id)
    *   }
    * })
    */
  floor_id: {
    /**
     * The name of this field.
     */
    name: 'floor_id'
  
    /**
     * The type of this field.
     */
    type: 'ID' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNonNullDef<'ID' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'ID\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'ID\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'floors', 'floor_id'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `floors.floor_number`.
    *
    * ### ️⚠️ You have not written documentation for model floors
    *
    * Replace this default advisory JSDoc with your own documentation about model floors
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model floors {
    *   /// Lorem ipsum dolor sit amet.
    *   floor_number  Int
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { floors } from 'nexus-prisma'
    *
    * objectType({
    *   name: floors.$name
    *   description: floors.$description
    *   definition(t) {
    *     t.field(floors.floor_number)
    *   }
    * })
    */
  floor_number: {
    /**
     * The name of this field.
     */
    name: 'floor_number'
  
    /**
     * The type of this field.
     */
    type: 'Int' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNonNullDef<'Int' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'Int\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'Int\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'floors', 'floor_number'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `floors.rooms`.
    *
    * ### ️⚠️ You have not written documentation for model floors
    *
    * Replace this default advisory JSDoc with your own documentation about model floors
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model floors {
    *   /// Lorem ipsum dolor sit amet.
    *   rooms  rooms
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { floors } from 'nexus-prisma'
    *
    * objectType({
    *   name: floors.$name
    *   description: floors.$description
    *   definition(t) {
    *     t.field(floors.rooms)
    *   }
    * })
    */
  rooms: {
    /**
     * The name of this field.
     */
    name: 'rooms'
  
    /**
     * The type of this field.
     */
    type: 'rooms' extends NexusCore.GetGen<'allNamedTypes', string>
    ? (NexusCore.NexusListDef<'rooms' & NexusCore.GetGen<'allNamedTypes', string>> | NexusCore.NexusNonNullDef<'rooms' & NexusCore.GetGen<'allNamedTypes', string>>)
    : 'Warning/Error: The type \'rooms\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'rooms\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'floors', 'rooms'>
  }
}

/**
  * Generated Nexus `objectType` configuration based on your Prisma schema's model `rooms`.
  *
  * ### ️⚠️ You have not written documentation for model rooms
  *
  * Replace this default advisory JSDoc with your own documentation about model rooms
  * by documenting it in your Prisma schema. For example:
  *
  * ```prisma
  * /// Lorem ipsum dolor sit amet...
  * model rooms {
  *   foo  String
  * }
  * ```
  *
  * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
  *
  * @example
  *
  * import { objectType } from 'nexus'
  * import { rooms } from 'nexus-prisma'
  *
  * objectType({
  *   name: rooms.$name
  *   description: rooms.$description
  *   definition(t) {
  *     t.field(rooms.id)
  *   }
  * })
  */
export interface rooms {
  $name: 'rooms'
  $description: undefined
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.uuid`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   uuid  String
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.uuid)
    *   }
    * })
    */
  uuid: {
    /**
     * The name of this field.
     */
    name: 'uuid'
  
    /**
     * The type of this field.
     */
    type: 'ID' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNonNullDef<'ID' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'ID\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'ID\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'uuid'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.id`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   id  Int?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.id)
    *   }
    * })
    */
  id: {
    /**
     * The name of this field.
     */
    name: 'id'
  
    /**
     * The type of this field.
     */
    type: 'Int' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'Int' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'Int\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'Int\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'id'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.n_salle`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   n_salle  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.n_salle)
    *   }
    * })
    */
  n_salle: {
    /**
     * The name of this field.
     */
    name: 'n_salle'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'n_salle'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.shape_leng`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   shape_leng  Float?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.shape_leng)
    *   }
    * })
    */
  shape_leng: {
    /**
     * The name of this field.
     */
    name: 'shape_leng'
  
    /**
     * The type of this field.
     */
    type: 'Float' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'Float' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'Float\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'Float\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'shape_leng'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.shape_area`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   shape_area  Float?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.shape_area)
    *   }
    * })
    */
  shape_area: {
    /**
     * The name of this field.
     */
    name: 'shape_area'
  
    /**
     * The type of this field.
     */
    type: 'Float' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'Float' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'Float\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'Float\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'shape_area'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.nom`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   nom  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.nom)
    *   }
    * })
    */
  nom: {
    /**
     * The name of this field.
     */
    name: 'nom'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'nom'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.domaine`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   domaine  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.domaine)
    *   }
    * })
    */
  domaine: {
    /**
     * The name of this field.
     */
    name: 'domaine'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'domaine'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.n_bureau`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   n_bureau  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.n_bureau)
    *   }
    * })
    */
  n_bureau: {
    /**
     * The name of this field.
     */
    name: 'n_bureau'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'n_bureau'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.floor_number`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   floor_number  Int?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.floor_number)
    *   }
    * })
    */
  floor_number: {
    /**
     * The name of this field.
     */
    name: 'floor_number'
  
    /**
     * The type of this field.
     */
    type: 'Int' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'Int' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'Int\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'Int\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'floor_number'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.floor_id`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   floor_id  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.floor_id)
    *   }
    * })
    */
  floor_id: {
    /**
     * The name of this field.
     */
    name: 'floor_id'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'floor_id'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.bookings`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   bookings  bookings
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.bookings)
    *   }
    * })
    */
  bookings: {
    /**
     * The name of this field.
     */
    name: 'bookings'
  
    /**
     * The type of this field.
     */
    type: 'bookings' extends NexusCore.GetGen<'allNamedTypes', string>
    ? (NexusCore.NexusListDef<'bookings' & NexusCore.GetGen<'allNamedTypes', string>> | NexusCore.NexusNonNullDef<'bookings' & NexusCore.GetGen<'allNamedTypes', string>>)
    : 'Warning/Error: The type \'bookings\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'bookings\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'bookings'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `rooms.floors`.
    *
    * ### ️⚠️ You have not written documentation for model rooms
    *
    * Replace this default advisory JSDoc with your own documentation about model rooms
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model rooms {
    *   /// Lorem ipsum dolor sit amet.
    *   floors  floors?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { rooms } from 'nexus-prisma'
    *
    * objectType({
    *   name: rooms.$name
    *   description: rooms.$description
    *   definition(t) {
    *     t.field(rooms.floors)
    *   }
    * })
    */
  floors: {
    /**
     * The name of this field.
     */
    name: 'floors'
  
    /**
     * The type of this field.
     */
    type: 'floors' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'floors' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'floors\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'floors\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'rooms', 'floors'>
  }
}

/**
  * Generated Nexus `objectType` configuration based on your Prisma schema's model `spatial_ref_sys`.
  *
  * ### ️⚠️ You have not written documentation for model spatial_ref_sys
  *
  * Replace this default advisory JSDoc with your own documentation about model spatial_ref_sys
  * by documenting it in your Prisma schema. For example:
  *
  * ```prisma
  * /// Lorem ipsum dolor sit amet...
  * model spatial_ref_sys {
  *   foo  String
  * }
  * ```
  *
  * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
  *
  * @example
  *
  * import { objectType } from 'nexus'
  * import { spatial_ref_sys } from 'nexus-prisma'
  *
  * objectType({
  *   name: spatial_ref_sys.$name
  *   description: spatial_ref_sys.$description
  *   definition(t) {
  *     t.field(spatial_ref_sys.id)
  *   }
  * })
  */
export interface spatial_ref_sys {
  $name: 'spatial_ref_sys'
  $description: undefined
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `spatial_ref_sys.srid`.
    *
    * ### ️⚠️ You have not written documentation for model spatial_ref_sys
    *
    * Replace this default advisory JSDoc with your own documentation about model spatial_ref_sys
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model spatial_ref_sys {
    *   /// Lorem ipsum dolor sit amet.
    *   srid  Int
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { spatial_ref_sys } from 'nexus-prisma'
    *
    * objectType({
    *   name: spatial_ref_sys.$name
    *   description: spatial_ref_sys.$description
    *   definition(t) {
    *     t.field(spatial_ref_sys.srid)
    *   }
    * })
    */
  srid: {
    /**
     * The name of this field.
     */
    name: 'srid'
  
    /**
     * The type of this field.
     */
    type: 'Int' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNonNullDef<'Int' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'Int\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'Int\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'spatial_ref_sys', 'srid'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `spatial_ref_sys.auth_name`.
    *
    * ### ️⚠️ You have not written documentation for model spatial_ref_sys
    *
    * Replace this default advisory JSDoc with your own documentation about model spatial_ref_sys
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model spatial_ref_sys {
    *   /// Lorem ipsum dolor sit amet.
    *   auth_name  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { spatial_ref_sys } from 'nexus-prisma'
    *
    * objectType({
    *   name: spatial_ref_sys.$name
    *   description: spatial_ref_sys.$description
    *   definition(t) {
    *     t.field(spatial_ref_sys.auth_name)
    *   }
    * })
    */
  auth_name: {
    /**
     * The name of this field.
     */
    name: 'auth_name'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'spatial_ref_sys', 'auth_name'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `spatial_ref_sys.auth_srid`.
    *
    * ### ️⚠️ You have not written documentation for model spatial_ref_sys
    *
    * Replace this default advisory JSDoc with your own documentation about model spatial_ref_sys
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model spatial_ref_sys {
    *   /// Lorem ipsum dolor sit amet.
    *   auth_srid  Int?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { spatial_ref_sys } from 'nexus-prisma'
    *
    * objectType({
    *   name: spatial_ref_sys.$name
    *   description: spatial_ref_sys.$description
    *   definition(t) {
    *     t.field(spatial_ref_sys.auth_srid)
    *   }
    * })
    */
  auth_srid: {
    /**
     * The name of this field.
     */
    name: 'auth_srid'
  
    /**
     * The type of this field.
     */
    type: 'Int' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'Int' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'Int\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'Int\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'spatial_ref_sys', 'auth_srid'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `spatial_ref_sys.srtext`.
    *
    * ### ️⚠️ You have not written documentation for model spatial_ref_sys
    *
    * Replace this default advisory JSDoc with your own documentation about model spatial_ref_sys
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model spatial_ref_sys {
    *   /// Lorem ipsum dolor sit amet.
    *   srtext  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { spatial_ref_sys } from 'nexus-prisma'
    *
    * objectType({
    *   name: spatial_ref_sys.$name
    *   description: spatial_ref_sys.$description
    *   definition(t) {
    *     t.field(spatial_ref_sys.srtext)
    *   }
    * })
    */
  srtext: {
    /**
     * The name of this field.
     */
    name: 'srtext'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'spatial_ref_sys', 'srtext'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `spatial_ref_sys.proj4text`.
    *
    * ### ️⚠️ You have not written documentation for model spatial_ref_sys
    *
    * Replace this default advisory JSDoc with your own documentation about model spatial_ref_sys
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model spatial_ref_sys {
    *   /// Lorem ipsum dolor sit amet.
    *   proj4text  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { spatial_ref_sys } from 'nexus-prisma'
    *
    * objectType({
    *   name: spatial_ref_sys.$name
    *   description: spatial_ref_sys.$description
    *   definition(t) {
    *     t.field(spatial_ref_sys.proj4text)
    *   }
    * })
    */
  proj4text: {
    /**
     * The name of this field.
     */
    name: 'proj4text'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'spatial_ref_sys', 'proj4text'>
  }
}

/**
  * Generated Nexus `objectType` configuration based on your Prisma schema's model `bookings`.
  *
  * ### ️⚠️ You have not written documentation for model bookings
  *
  * Replace this default advisory JSDoc with your own documentation about model bookings
  * by documenting it in your Prisma schema. For example:
  *
  * ```prisma
  * /// Lorem ipsum dolor sit amet...
  * model bookings {
  *   foo  String
  * }
  * ```
  *
  * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
  *
  * @example
  *
  * import { objectType } from 'nexus'
  * import { bookings } from 'nexus-prisma'
  *
  * objectType({
  *   name: bookings.$name
  *   description: bookings.$description
  *   definition(t) {
  *     t.field(bookings.id)
  *   }
  * })
  */
export interface bookings {
  $name: 'bookings'
  $description: undefined
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `bookings.uuid`.
    *
    * ### ️⚠️ You have not written documentation for model bookings
    *
    * Replace this default advisory JSDoc with your own documentation about model bookings
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model bookings {
    *   /// Lorem ipsum dolor sit amet.
    *   uuid  String
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { bookings } from 'nexus-prisma'
    *
    * objectType({
    *   name: bookings.$name
    *   description: bookings.$description
    *   definition(t) {
    *     t.field(bookings.uuid)
    *   }
    * })
    */
  uuid: {
    /**
     * The name of this field.
     */
    name: 'uuid'
  
    /**
     * The type of this field.
     */
    type: 'ID' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNonNullDef<'ID' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'ID\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'ID\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'bookings', 'uuid'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `bookings.created_at`.
    *
    * ### ️⚠️ You have not written documentation for model bookings
    *
    * Replace this default advisory JSDoc with your own documentation about model bookings
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model bookings {
    *   /// Lorem ipsum dolor sit amet.
    *   created_at  DateTime?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { bookings } from 'nexus-prisma'
    *
    * objectType({
    *   name: bookings.$name
    *   description: bookings.$description
    *   definition(t) {
    *     t.field(bookings.created_at)
    *   }
    * })
    */
  created_at: {
    /**
     * The name of this field.
     */
    name: 'created_at'
  
    /**
     * The type of this field.
     */
    type: 'DateTime' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'DateTime' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'DateTime\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'DateTime\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'bookings', 'created_at'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `bookings.start_date`.
    *
    * ### ️⚠️ You have not written documentation for model bookings
    *
    * Replace this default advisory JSDoc with your own documentation about model bookings
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model bookings {
    *   /// Lorem ipsum dolor sit amet.
    *   start_date  DateTime?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { bookings } from 'nexus-prisma'
    *
    * objectType({
    *   name: bookings.$name
    *   description: bookings.$description
    *   definition(t) {
    *     t.field(bookings.start_date)
    *   }
    * })
    */
  start_date: {
    /**
     * The name of this field.
     */
    name: 'start_date'
  
    /**
     * The type of this field.
     */
    type: 'DateTime' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'DateTime' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'DateTime\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'DateTime\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'bookings', 'start_date'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `bookings.end_date`.
    *
    * ### ️⚠️ You have not written documentation for model bookings
    *
    * Replace this default advisory JSDoc with your own documentation about model bookings
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model bookings {
    *   /// Lorem ipsum dolor sit amet.
    *   end_date  DateTime?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { bookings } from 'nexus-prisma'
    *
    * objectType({
    *   name: bookings.$name
    *   description: bookings.$description
    *   definition(t) {
    *     t.field(bookings.end_date)
    *   }
    * })
    */
  end_date: {
    /**
     * The name of this field.
     */
    name: 'end_date'
  
    /**
     * The type of this field.
     */
    type: 'DateTime' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'DateTime' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'DateTime\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'DateTime\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'bookings', 'end_date'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `bookings.room_uuid`.
    *
    * ### ️⚠️ You have not written documentation for model bookings
    *
    * Replace this default advisory JSDoc with your own documentation about model bookings
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model bookings {
    *   /// Lorem ipsum dolor sit amet.
    *   room_uuid  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { bookings } from 'nexus-prisma'
    *
    * objectType({
    *   name: bookings.$name
    *   description: bookings.$description
    *   definition(t) {
    *     t.field(bookings.room_uuid)
    *   }
    * })
    */
  room_uuid: {
    /**
     * The name of this field.
     */
    name: 'room_uuid'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'bookings', 'room_uuid'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `bookings.user_email`.
    *
    * ### ️⚠️ You have not written documentation for model bookings
    *
    * Replace this default advisory JSDoc with your own documentation about model bookings
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model bookings {
    *   /// Lorem ipsum dolor sit amet.
    *   user_email  String?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { bookings } from 'nexus-prisma'
    *
    * objectType({
    *   name: bookings.$name
    *   description: bookings.$description
    *   definition(t) {
    *     t.field(bookings.user_email)
    *   }
    * })
    */
  user_email: {
    /**
     * The name of this field.
     */
    name: 'user_email'
  
    /**
     * The type of this field.
     */
    type: 'String' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'String' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'String\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'String\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'bookings', 'user_email'>
  }
  /**
    * Generated Nexus `t.field` configuration based on your Prisma schema's model-field `bookings.rooms`.
    *
    * ### ️⚠️ You have not written documentation for model bookings
    *
    * Replace this default advisory JSDoc with your own documentation about model bookings
    * by documenting it in your Prisma schema. For example:
    * ```prisma
    * model bookings {
    *   /// Lorem ipsum dolor sit amet.
    *   rooms  rooms?
    * }
    * ```
    *
    * Learn more about documentation comments in Prisma schema files [here](https://www.prisma.io/docs/concepts/components/prisma-schema#comments).
    *
    * @example
    *
    * import { objectType } from 'nexus'
    * import { bookings } from 'nexus-prisma'
    *
    * objectType({
    *   name: bookings.$name
    *   description: bookings.$description
    *   definition(t) {
    *     t.field(bookings.rooms)
    *   }
    * })
    */
  rooms: {
    /**
     * The name of this field.
     */
    name: 'rooms'
  
    /**
     * The type of this field.
     */
    type: 'rooms' extends NexusCore.GetGen<'allNamedTypes', string>
    ? NexusCore.NexusNullDef<'rooms' & NexusCore.GetGen<'allNamedTypes', string>>
    : 'Warning/Error: The type \'rooms\' is not amoung the union of GetGen<\'allNamedTypes\', string>. This means that either: 1) You need to run nexus typegen reflection. 2) You need to add the type \'rooms\' to your GraphQL API.'
  
    /**
     * The documentation of this field.
     */
    description: undefined
  
    /**
     * The resolver of this field
     */
    resolve: NexusCore.FieldResolver<'bookings', 'rooms'>
  }
}

// Enums

// N/A –– You have not defined any enums in your Prisma schema file.


//
//
// TERMS
// TERMS
// TERMS
// TERMS
//
//

//
//
// EXPORTS: PRISMA MODELS
// EXPORTS: PRISMA MODELS
// EXPORTS: PRISMA MODELS
// EXPORTS: PRISMA MODELS
//
//

export const floors: floors

export const rooms: rooms

export const spatial_ref_sys: spatial_ref_sys

export const bookings: bookings

//
//
// EXPORTS: PRISMA ENUMS
// EXPORTS: PRISMA ENUMS
// EXPORTS: PRISMA ENUMS
// EXPORTS: PRISMA ENUMS
//
//

// N/A –– You have not defined any enums in your Prisma schema file.

//
//
// EXPORTS: OTHER
// EXPORTS: OTHER
// EXPORTS: OTHER
// EXPORTS: OTHER
//
//

import type { Settings } from 'nexus-prisma/dist-cjs/generator/Settings/index'

/**
 * Adjust Nexus Prisma's [runtime settings](https://pris.ly/nexus-prisma/docs/settings/runtime).
 *
 * @example
 *
 *     import { PrismaClient } from '@prisma/client'
 *     import { ApolloServer } from 'apollo-server'
 *     import { makeSchema } from 'nexus'
 *     import { User, Post, $settings } from 'nexus-prisma'
 *
 *     new ApolloServer({
 *       schema: makeSchema({
 *         types: [],
 *       }),
 *       context() {
 *         return {
 *           db: new PrismaClient(), // <-- You put Prisma client on the "db" context property
 *         }
 *       },
 *     })
 *
 *     $settings({
 *       prismaClientContextField: 'db', // <-- Tell Nexus Prisma
 *     })
 *
 * @remarks This is _different_ than Nexus Prisma's [_gentime_ settings](https://pris.ly/nexus-prisma/docs/settings/gentime).
 */
export const $settings: Settings.Runtime.Manager['change']
