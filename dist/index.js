import { server } from "./api/server.js";
import { startStandaloneServer } from "@apollo/server/standalone";
import { context } from "./api/context.js";
const port = Number.parseInt(process.env.PORT) || 4000;
const { url } = await startStandaloneServer(server, {
    listen: { port },
    context: async () => {
        return context;
    },
});
console.log(`🚀  Server ready at: ${url}`);
