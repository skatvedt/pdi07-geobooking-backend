import { PrismaClient } from "@prisma/client";
export interface Context {
    db: PrismaClient;
}
export declare const context: {
    db: PrismaClient<import(".prisma/client").Prisma.PrismaClientOptions, never, import(".prisma/client").Prisma.RejectOnNotFound | import(".prisma/client").Prisma.RejectPerOperation>;
};
