import { join } from "path";
import path from "path";
import { fileURLToPath } from "url";
import { asNexusMethod, makeSchema } from "nexus";
import * as types from "./index.js";
import { DateTimeResolver } from "graphql-scalars";
const filename = fileURLToPath(import.meta.url);
const dirname = path.dirname(filename);
export const DateTime = asNexusMethod(DateTimeResolver, "datetime");
export const schema = makeSchema({
    types: [types, DateTime],
    outputs: {
        typegen: join(dirname, "../..", "nexus-typegen.ts"),
        schema: join(dirname, "../..", "schema.graphql"),
    },
    contextType: {
        module: join(dirname, "./context.ts"),
        export: "Context",
    },
});
