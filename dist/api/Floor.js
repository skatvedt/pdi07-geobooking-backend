import { objectType, extendType, list, nonNull, } from "nexus";
export const Floor = objectType({
    name: "Floor",
    definition(t) {
        t.string("floor_id");
        t.int("floor_number");
        t.field("rooms", {
            type: nonNull(list("Room")),
            resolve(_root, _args, ctx) {
                return ctx.db.rooms.findMany({ where: { floor_id: _root.floor_id } });
            },
        });
    },
});
export const FloorQuery = extendType({
    type: "Query",
    definition(t) {
        t.field("floors", {
            type: nonNull(list("Floor")),
            resolve(_root, _args, ctx) {
                return ctx.db.floors.findMany();
            },
        });
    },
});
