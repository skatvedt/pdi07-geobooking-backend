import { Prisma } from "@prisma/client";
import { objectType, extendType, list, nonNull, } from "nexus";
export const Room = objectType({
    name: "Room",
    definition(t) {
        t.string("uuid");
        t.int("id");
        t.string("n_salle");
        t.float("shape_leng");
        t.float("shape_area");
        t.string("nom");
        t.string("domaine");
        t.string("n_bureau");
        t.int("floor_number");
        t.string("floor_id");
        t.field("geom", {
            type: "String",
            resolve(_root, _args, ctx) {
                return ctx.db.$queryRaw(Prisma.sql `SELECT ST_AsGeoJSON(geom) FROM rooms WHERE uuid = '${_root.uuid}'`);
            },
        });
        t.field("floor", {
            type: "Floor",
            resolve(_root, _args, ctx) {
                return ctx.db.floors.findUnique({
                    where: { floor_id: _root.floor_id },
                });
            },
        });
    },
});
export const RoomQuery = extendType({
    type: "Query",
    definition(t) {
        t.field("rooms", {
            type: nonNull(list("Room")),
            resolve(_root, _args, ctx) {
                return ctx.db.rooms.findMany();
            },
        });
    },
});
